import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { HttpService } from 'src/app/service/http.service';
import { AddMovieComponent } from '../movie/add-movie/add-movie.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  movieList: Movie[]

  constructor(
    private http: HttpService,
    private router: Router,
    private modalService: NgbModal
  ) {
    this.movieList = []

  }

  ngOnInit(): void {
    this.init();
  }

  async init() {
    this.movieList = await this.http.getMovieList();
  }


  onClickMovie(id: number | undefined): void {
    if (id) {
      this.router.navigateByUrl("/app/movie/" + id)
    }
  }

  addNewMovie() {
    let modalRef = this.modalService.open(AddMovieComponent);
    modalRef.result.then((data: Movie) => {
      this.movieList.splice(0, 0, data)
    })
  }

}
