package grpcclient

import (
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/config"
	"google.golang.org/grpc"
)

// ConnectUserService connects to the user service
func ConnectUserService() (*grpc.ClientConn, error) {
	return grpc.Dial(config.GetGrpcUserService(), grpc.WithInsecure())

}

// ConnectMovieService connects to the movieservice
func ConnectMovieService() (*grpc.ClientConn, error) {
	return grpc.Dial(config.GetGrpcMovieService(), grpc.WithInsecure())
}
