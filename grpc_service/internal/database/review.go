package database

import (
	"errors"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

// Review data model for Reviews Table
type Review struct {
	gorm.Model
	MovieID     uint
	Comments    string
	Rating      int
	CreatedByID uint
	UpdatedByID uint

	CreatedBy string `gorm:"->"`
	UpdatedBy string `gorm:"->"`
}

// Add Adds a new review to the database
func (review *Review) Add() error {
	tempReview := &Review{}
	err := tempReview.Get(review.MovieID, review.CreatedByID)

	if err == nil {
		return status.Error(codes.AlreadyExists, "Review already exists")
	}
	if !errors.Is(err, gorm.ErrRecordNotFound) {
		return err
	}
	db.postgres.Create(&review)

	return nil
}

// Update updates the review in the database
func (review *Review) Update(newReview Review) error {

	err := db.postgres.Model(&review).Updates(newReview).Error
	if err != nil {
		return err
	}

	return nil
}

// Delete Deletes the respective review in the database
func (review *Review) Delete() error {

	if review.ID == 0 {
		return status.Error(codes.InvalidArgument, "invalid review ID")
	}

	result := db.postgres.Delete(review)

	if result.Error != nil {
		return result.Error
	}

	if result.RowsAffected == 0 {
		return status.Error(codes.InvalidArgument, "user couldnot be deleted")
	}

	return nil
}

// Get Gets a single review from DB for the respective movie and User
func (review *Review) Get(movieID uint, userID uint) error {

	err := db.postgres.
		Select("reviews.*, creatUser.name as created_by, updateUser.name as updated_by").
		Joins("left join users as creatUser on reviews.created_by_id = creatUser.id").
		Joins("left join users as updateUser on reviews.updated_by_id = updateUser.id").
		Where("movie_id = ? AND created_by_id = ?", movieID, userID).First(&review).Error
	if err != nil {
		return err
	}

	return nil
}

// GetReviews gets all the reviews of the respective movie
func (review *Review) GetReviews(movieID uint) ([]Review, error) {
	var reviews []Review
	err := db.postgres.
		Select("reviews.*, creatUser.name as created_by, updateUser.name as updated_by").
		Joins("left join users as creatUser on reviews.created_by_id = creatUser.id").
		Joins("left join users as updateUser on reviews.updated_by_id = updateUser.id").
		Where("movie_id = ?", movieID).Find(&reviews).Error
	if err != nil {
		return nil, err
	}

	return reviews, nil
}
