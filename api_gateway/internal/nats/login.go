package nats

import (
	"context"
	"time"

	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// Login ...
func (c *Client) Login(ctx context.Context, req *services.LogInRequest) (*services.LogInResponse, error) {
	err := c.connect()
	if err != nil {
		return nil, err
	}
	defer c.close()

	resp := &services.LogInResponse{}
	if err := c.encodedConn.Request("login", req, &resp, 10*time.Second); err != nil {
		return nil, err
	}
	return resp, nil
}
