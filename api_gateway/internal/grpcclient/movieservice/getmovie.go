package movieservice

import (
	"context"
	"time"

	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/grpcclient"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// GetMovie ...
func GetMovie(ctx context.Context, req *services.GetMovieRequest) (*services.GetMovieResponse, error) {
	movieConn, err := grpcclient.ConnectMovieService()
	defer movieConn.Close()
	if err != nil {
		return nil, err
	}

	movieServer := services.NewMovieServiceClient(movieConn)

	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	resp, err := movieServer.GetMovie(ctx, req)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
