package security

import (
	"log"
	"net/http"

	"github.com/adam-hanna/jwt-auth/jwt"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/tokenmanager"
)

// TokenResponder ...
type TokenResponder struct {
	claims    *jwt.ClaimsType
	responder middleware.Responder
}

// NewTokenResponder ...
func NewTokenResponder(claims *jwt.ClaimsType, responder middleware.Responder) *TokenResponder {
	return &TokenResponder{
		responder: responder,
		claims:    claims,
	}
}

// WriteResponse ...
func (tr *TokenResponder) WriteResponse(rw http.ResponseWriter, p runtime.Producer) {
	err := tokenmanager.TokenHandler.IssueNewTokens(rw, tr.claims)
	if err != nil {
		log.Printf("Issue Token ERROR: %v", err)
	}

	tr.responder.WriteResponse(rw, p)
}
