package usermethods

import (
	"context"
	"strconv"

	"github.com/adam-hanna/jwt-auth/jwt"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/grpcclient/userservice"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/security"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/models"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/user"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// Signup ...
func Signup(params user.PostSignupParams) middleware.Responder {
	response, err := userservice.Signup(context.Background(), &services.AddUserRequest{
		Name:     *params.Data.Name,
		Email:    *params.Data.Email,
		Password: *params.Data.Password,
	})

	if err != nil {
		return user.NewPostSignupBadRequest().WithPayload(err.Error())
	}

	resp := user.NewPostSignupOK()
	claims := jwt.ClaimsType{}
	claims.StandardClaims.Id = strconv.Itoa(int(response.Id))
	return security.NewTokenResponder(&claims, resp.WithPayload(&models.AddUserResp{
		Email: swag.String(response.Email),
		ID:    swag.Int64(int64(response.Id)),
		Name:  swag.String(response.Name),
	}))
}
