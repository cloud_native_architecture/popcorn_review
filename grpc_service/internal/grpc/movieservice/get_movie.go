package movieservice

import (
	"context"

	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/database"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// GetMovie ...
func (s *Server) GetMovie(ctx context.Context, req *services.GetMovieRequest) (*services.GetMovieResponse, error) {

	movie := &database.Movie{}
	movie.ID = uint(req.GetId())

	err := movie.Get()
	if err != nil {
		return nil, err
	}

	reviews, err := (&database.Review{}).GetReviews(movie.ID)
	if err != nil {
		return nil, err
	}

	reply := &services.GetMovieResponse{
		Id:           int32(movie.ID),
		Name:         movie.Name,
		DirectorName: movie.DirectorName,
		AvgReview:    movie.AvgRating,
		Poster:       movie.PosterImg,
		Cast:         make([]string, 0),
		Genre:        make([]string, 0),
		Reviews:      make([]*services.GetMovieResponse_Review, 0),
	}

	for _, cast := range movie.Casts {
		reply.Cast = append(reply.Cast, cast.Name)
	}

	for _, genre := range movie.Genres {
		reply.Genre = append(reply.Genre, genre.Name)
	}

	for _, review := range reviews {
		reply.Reviews = append(reply.Reviews, &services.GetMovieResponse_Review{
			Rating:    int32(review.Rating),
			CreatedAt: timestamppb.New(review.CreatedAt),
			Comments:  review.Comments,
			Name:      review.CreatedBy,
		})
	}

	return reply, nil
}
