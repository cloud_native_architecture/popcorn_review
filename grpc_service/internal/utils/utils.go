package utils

import (
	"golang.org/x/crypto/bcrypt"
)

// EncryptPassword encrypts the given password
func EncryptPassword(pwd string) (string, error) {
	encryptedPwd, err := bcrypt.GenerateFromPassword([]byte(pwd), 10)
	if err != nil {
		return "", err
	}
	return string(encryptedPwd), nil
}

// VerifyPassword verify the given password with the encrypted password
func VerifyPassword(pwd string, encryptedPwd string) error {
	return bcrypt.CompareHashAndPassword([]byte(encryptedPwd), []byte(pwd))
}
