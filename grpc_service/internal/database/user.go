package database

import (
	"errors"

	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/utils"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

// User Data model to store Users Table
type User struct {
	gorm.Model
	Name     string
	Email    string
	Password string
}

// Add Adds new users to DB
func (user *User) Add() error {
	pwd, err := utils.EncryptPassword(user.Password)
	if err != nil {
		return status.Error(codes.Internal, "Failed to encrypt password")
	}
	user.Password = pwd
	// check if already exists
	err = (&User{Email: user.Email}).GetByEmail()
	if err == nil {
		return status.Error(codes.AlreadyExists, "User already exists")
	}
	if !errors.Is(err, gorm.ErrRecordNotFound) {
		return err
	}

	// add to database
	err = db.postgres.Create(&user).Error
	if err != nil {
		return err
	}

	return nil
}

// Get gets the respective user
func (user *User) Get() error {
	err := db.postgres.First(&user).Error
	if err != nil {
		return err
	}

	return nil
}

// GetByEmail gets teh user with the respective email
func (user *User) GetByEmail() error {
	err := db.postgres.Where("email = ?", user.Email).First(&user).Error
	if err != nil {
		return err
	}
	return nil
}

// Update updates the user in DB
func (user *User) Update(newUser User) error {

	err := db.postgres.Model(&user).Updates(newUser).Error
	if err != nil {
		return err
	}
	return nil
}

// Delete deletes the user from the DB
func (user *User) Delete() error {

	result := db.postgres.Delete(user)

	if result.Error != nil {
		return result.Error
	}
	if result.RowsAffected == 0 {
		return status.Error(codes.InvalidArgument, "user couldnot be deleted")
	}
	return nil
}
