import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { HttpService } from 'src/app/service/http.service';
import { StateService } from 'src/app/service/state.service';
import { AddMovieComponent } from '../add-movie/add-movie.component';
import { AddReviewComponent } from '../add-review/add-review.component';

@Component({
  selector: 'app-view-movie',
  templateUrl: './view-movie.component.html',
  styleUrls: ['./view-movie.component.scss']
})
export class ViewMovieComponent implements OnInit {

  movie: MovieInfo = {} as MovieInfo
  constructor(
    private http: HttpService,
    private state: StateService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.init(params['id'])
    })
  }

  async init(id: number) {
    this.movie = await this.http.getMovie(id);
  }

  delete(content: any) {
    this.modalService.open(content).result.then((del: boolean) => {
      if (del) {
        this.deleteMovie();
      }
    });
  }

  async deleteMovie(){
    if( this.movie.id){
      try{
        await this.http.deleteMovie(this.movie.id)
        this.router.navigateByUrl("/app");
      }catch{

      }

    }
  }

  addReview() {
    let modalRef = this.modalService.open(AddReviewComponent);
    modalRef.result.then((data: Review) => {
      data.movieId = this.movie.id
      this.postReview(data);
    })
  }

  async postReview(data: Review) {
    try {
      let review = await this.http.postAddReview(data);
      if (!this.movie.reviews) {
        this.movie.reviews = []
      }
      this.movie.reviews.push({
        rating: data.rating,
        comments: data.comments,
        movieId: data.movieId,
        name: this.state.userName,
        createdAt: review.createdAt,
        id: review.id
      });
    }
    catch {
      this.toastr.error('Review Already exists', "Failed to Add Review");
    }
  }
}
