package security

import (
	"encoding/base64"
	"errors"
	"strings"
)

// GetEmailAndPwd ...s
func GetEmailAndPwd(tokenString string) (string, string, error) {
	if !strings.Contains(tokenString, "Basic") {
		return "", "", errors.New("basic Authorization not found")
	}

	tokenString = strings.ReplaceAll(tokenString, "Basic ", "")

	tokenByte, err := base64.StdEncoding.DecodeString(tokenString)
	if err != nil {
		return "", "", errors.New("invalid username/Password encryption")
	}
	tokenString = string(tokenByte)
	fields := strings.Split(tokenString, ":")
	if len(fields) != 2 {
		return "", "", errors.New("invalid number of fields sent")
	}

	return fields[0], fields[1], nil
}
