package userservice_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/testutils"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

func TestLogIn(t *testing.T) {
	defer testutils.Setup("user_service")()

	ctx := context.Background()

	user := testutils.DummyUser()
	pwd := user.Password
	user.Add()

	resp, err := server.LogIn(ctx, &services.LogInRequest{
		Email:    user.Email,
		Password: pwd,
	})

	assert.NoError(t, err, "Failed to Login")
	assert.NotNil(t, resp)
	assert.NotEqual(t, resp.Id, 0, " User ID  must not be 0")
}
