package userservice

import (
	"context"

	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/database"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// AddUser ...
func (s *Server) AddUser(ctx context.Context, req *services.AddUserRequest) (*services.AddUserResponse, error) {
	user := &database.User{
		Name:     req.GetName(),
		Email:    req.GetEmail(),
		Password: req.GetPassword(),
	}

	err := user.Add()

	if err != nil {
		return nil, err
	}

	resp := &services.AddUserResponse{
		Id:    int32(user.ID),
		Name:  user.Name,
		Email: user.Email,
	}

	return resp, nil
}
