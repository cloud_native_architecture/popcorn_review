package movieservice

import (
	"context"

	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/database"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// DeleteMovie ...
func (s *Server) DeleteMovie(ctx context.Context, req *services.DeleteMovieRequest) (*services.DeleteMovieResponse, error) {

	movie := &database.Movie{}
	movie.ID = uint(req.GetId())

	err := movie.Delete()
	if err != nil {
		return nil, err
	}

	resp := &services.DeleteMovieResponse{
		Status: true,
	}
	return resp, nil
}
