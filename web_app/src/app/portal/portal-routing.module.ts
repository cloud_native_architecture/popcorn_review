import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ViewMovieComponent } from './movie/view-movie/view-movie.component';

const routes: Routes = [

  {
    path: '',
    component: DashboardComponent,
  },
  {
    path: "movie/:id",
    component: ViewMovieComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortalRoutingModule { }
