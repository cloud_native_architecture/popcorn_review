interface Review extends Indexable, Creatable {
    rating: number;
    name?: string;
    comments: string;
    movieId?: number
}