package userservice

import (
	"context"

	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/database"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/utils"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// LogIn ...
func (s *Server) LogIn(ctx context.Context, req *services.LogInRequest) (*services.LogInResponse, error) {
	user := &database.User{Email: req.GetEmail()}
	err := user.GetByEmail()
	if err != nil {
		return nil, err
	}
	err = utils.VerifyPassword(req.GetPassword(), user.Password)
	if err != nil {
		return nil, err
	}

	reply := &services.LogInResponse{
		Id:    int32(user.ID),
		Name:  user.Name,
		Email: user.Email,
	}

	return reply, nil
}
