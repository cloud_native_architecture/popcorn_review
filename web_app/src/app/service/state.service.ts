import { Injectable } from '@angular/core';
import { USER, X_CSRF_TOKEN } from '../models/data/constants';


@Injectable({
  providedIn: 'root'
})
export class StateService {
  constructor() { }

  public get userName() {
    let userStr = localStorage.getItem(USER);
    if (userStr) {
      let user: User = JSON.parse(userStr);
      return user.name;
    }
    return "";
  }

  public get loggedIn(): boolean {
    return this.token != null
  }

  public get token(): string | null {
    return localStorage.getItem(X_CSRF_TOKEN);
  }

  public set token(tkn: string | null) {
    if (tkn == null) {
      localStorage.removeItem(X_CSRF_TOKEN);
    } else {
      localStorage.setItem(X_CSRF_TOKEN, tkn);
    }
  }

  login(user: User): void {
    localStorage.setItem(USER, JSON.stringify(user));

  }

  logout(): void {
    localStorage.removeItem(X_CSRF_TOKEN);
    localStorage.removeItem(USER);
  }


}
