export let genreList: dropdown[] = [
    { id: 1, text: 'Comedy' },
    { id: 2, text: 'Drama' },
    { id: 3, text: 'Fantasy' },
    { id: 4, text: 'Horror' },
    { id: 5, text: 'Mystery' },
    { id: 6, text: 'Romance' },
    { id: 7, text: 'Thriller' },
    { id: 8, text: 'Western' },
    { id: 9, text: 'Action' }
]