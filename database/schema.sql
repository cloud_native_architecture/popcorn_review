CREATE DATABASE popcorn_review_template;

\c popcorn_review_template

CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar,
  "email" varchar UNIQUE,
  "password" varchar,
  "created_at" timestamp with time zone,
  "updated_at" timestamp with time zone,
  "deleted_at" timestamp with time zone
);

CREATE TABLE "movies" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar UNIQUE,
  "director_name" varchar,
  "poster_img" varchar,
  "created_by_id" int,
  "updated_by_id" int,
  "created_at" timestamp with time zone,
  "updated_at" timestamp with time zone,
  "deleted_at" timestamp with time zone
);

CREATE TABLE "genres" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar
);

CREATE TABLE "movie_genre" (
  "movie_id" int,
  "genre_id" int
);

CREATE TABLE "casts" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar UNIQUE,
  "created_by_id" int,
  "updated_by_id" int,
  "created_at" timestamp with time zone,
  "updated_at" timestamp with time zone,
  "deleted_at" timestamp with time zone
);

CREATE TABLE "movie_cast" (
  "movie_id" int,
  "cast_id" int
);

CREATE TABLE "reviews" (
  "id" SERIAL PRIMARY KEY,
  "movie_id" int,
  "comments" varchar,
  "rating" int,
  "created_by_id" int,
  "updated_by_id" int,
  "created_at" timestamp with time zone,
  "updated_at" timestamp with time zone,
  "deleted_at" timestamp with time zone
);

ALTER TABLE "movies" ADD FOREIGN KEY ("created_by_id") REFERENCES "users" ("id");

ALTER TABLE "movies" ADD FOREIGN KEY ("updated_by_id") REFERENCES "users" ("id");

ALTER TABLE "movie_genre" ADD FOREIGN KEY ("movie_id") REFERENCES "movies" ("id");

ALTER TABLE "movie_genre" ADD FOREIGN KEY ("genre_id") REFERENCES "genres" ("id");

ALTER TABLE "casts" ADD FOREIGN KEY ("created_by_id") REFERENCES "users" ("id");

ALTER TABLE "casts" ADD FOREIGN KEY ("updated_by_id") REFERENCES "users" ("id");

ALTER TABLE "movie_cast" ADD FOREIGN KEY ("movie_id") REFERENCES "movies" ("id");

ALTER TABLE "movie_cast" ADD FOREIGN KEY ("cast_id") REFERENCES "casts" ("id");

ALTER TABLE "reviews" ADD FOREIGN KEY ("movie_id") REFERENCES "movies" ("id");

ALTER TABLE "reviews" ADD FOREIGN KEY ("created_by_id") REFERENCES "users" ("id");

ALTER TABLE "reviews" ADD FOREIGN KEY ("updated_by_id") REFERENCES "users" ("id");


-- Default Data for Genre

INSERT INTO public.genres VALUES (1, 'Comedy');
INSERT INTO public.genres VALUES (2, 'Drama');
INSERT INTO public.genres VALUES (3, 'Fantasy');
INSERT INTO public.genres VALUES (4, 'Horror');
INSERT INTO public.genres VALUES (5, 'Mystery');
INSERT INTO public.genres VALUES (6, 'Romance');
INSERT INTO public.genres VALUES (7, 'Thriller');
INSERT INTO public.genres VALUES (8, 'Western');
INSERT INTO public.genres VALUES (9, 'Action');


CREATE DATABASE popcorn_review WITH TEMPLATE popcorn_review_template;