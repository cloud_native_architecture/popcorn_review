package movieservice

import (
	"context"

	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/database"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// GetMovies ...
func (s *Server) GetMovies(ctx context.Context, req *services.GetMoviesRequest) (*services.GetMoviesResponse, error) {

	var movies []database.Movie
	movies, err := (&database.Movie{}).GetAll()
	if err != nil {
		return nil, err
	}

	respMovies := make([]*services.GetMoviesResponse_Movie, 0)

	for _, movie := range movies {
		respMovies = append(respMovies, &services.GetMoviesResponse_Movie{
			Id:           int32(movie.ID),
			Name:         movie.Name,
			DirectorName: movie.DirectorName,
			AvgReview:    float32(movie.AvgRating),
			PosterImg:    movie.PosterImg,
		})
	}

	reply := &services.GetMoviesResponse{
		Movies: respMovies,
	}

	return reply, nil
}
