package moviemethods

import (
	"context"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/grpcclient/movieservice"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/models"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/movie"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// GetMovie ...
func GetMovie(params movie.GetMovieIDParams) middleware.Responder {

	resp, err := movieservice.GetMovie(context.Background(), &services.GetMovieRequest{
		Id: int32(params.ID),
	})

	if err != nil {
		return movie.NewGetMovieIDBadRequest().WithPayload(err.Error())
	}

	reply := &models.GetMovieResp{
		ID:           int64(resp.GetId()),
		Name:         resp.GetName(),
		DirectorName: resp.GetDirectorName(),
		AvgReview:    resp.GetAvgReview(),
		PosterImg:    resp.GetPoster(),
		Cast:         make([]string, 0),
		Genre:        make([]string, 0),
		Reviews:      make([]*models.GetMovieRespReviewsItems0, 0),
	}
	for _, cast := range resp.Cast {
		reply.Cast = append(reply.Cast, cast)
	}
	for _, genre := range resp.Genre {
		reply.Genre = append(reply.Genre, genre)
	}
	for _, review := range resp.Reviews {
		reply.Reviews = append(reply.Reviews, &models.GetMovieRespReviewsItems0{
			Name:      review.Name,
			CreatedAt: strfmt.DateTime(review.CreatedAt.AsTime()),
			Comments:  review.Comments,
			Rating:    review.Rating,
		})
	}
	return movie.NewGetMovieIDOK().WithPayload(reply)
}
