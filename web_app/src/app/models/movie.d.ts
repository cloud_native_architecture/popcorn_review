interface Movie extends Indexable {
    name: string
    directorName: string
    avgReview?: number
    posterImg?: string
}

interface MovieInfo extends Movie {
    cast: string[]
    genre: string[]
    reviews?: Review[]
}


interface AddMovie extends Movie {
    cast: string[]
    genre: number[]
    image: string
}