package userservice

import (
	"context"
	"time"

	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/grpcclient"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// Signup ...
func Signup(ctx context.Context, req *services.AddUserRequest) (*services.AddUserResponse, error) {
	userConn, err := grpcclient.ConnectUserService()
	defer userConn.Close()
	if err != nil {
		return nil, err
	}

	userServer := services.NewUserServiceClient(userConn)

	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	resp, err := userServer.AddUser(ctx, req)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
