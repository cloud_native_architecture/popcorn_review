import { HttpErrorResponse, HttpEvent, HttpEventType, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StateService } from './state.service';
import { tap } from "rxjs/operators";
import { X_CSRF_TOKEN } from '../models/data/constants';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddMovieComponent } from '../portal/movie/add-movie/add-movie.component';
@Injectable({
  providedIn: 'root'
})

export class HttpInterceptorService implements HttpInterceptor {
  constructor(
    private router: Router,
    private state: StateService,
    private modalService: NgbModal
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
    var request = req.clone({ headers: req.headers, withCredentials: true });
    if (req.url.startsWith(environment.apiEndpoint)) {
      if (this.state.token != null) {
        request = request.clone({ headers: req.headers.set(X_CSRF_TOKEN, this.state.token) });
      }
    }
    return next.handle(request).pipe(tap(
      (event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Response: {
            if (event.headers.get(X_CSRF_TOKEN)) {
              this.state.token = event.headers.get(X_CSRF_TOKEN)
            }
            break;
          }
        }
      },
      (errResp: HttpErrorResponse) => {
        console.log(errResp, errResp.status)
        switch (errResp.status) {
          case 401:
          case 0: {
            console.log("Unauthorized")
            this.state.logout();
            this.modalService.dismissAll();
            this.router.navigateByUrl("/login");
            break;
          }
          case 403: {
            console.log("Forbidden")
            break;
          }
        }
      }));

  }
}

export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
];

