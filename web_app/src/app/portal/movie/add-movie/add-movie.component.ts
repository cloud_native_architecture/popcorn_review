import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { NgxImageCompressService } from 'ngx-image-compress';
import { genreList } from 'src/app/models/data/genre';
import { HttpService } from 'src/app/service/http.service';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.scss']
})
export class AddMovieComponent implements OnInit {
  addMovieForm: FormGroup = this.fb.group({});
  genreDropdown: dropdown[] = [];
  castDropdown: dropdown[] = [];
  dropdownSettings: IDropdownSettings = {};
  others: boolean = false;
  imagePath?: SafeResourceUrl

  get imagedata(): string {
    return this.addMovieForm.get('image')?.value
  }

  set imagedata(val: string) {
    this.addMovieForm.get('image')?.setValue(val)
  }

  constructor(
    private http: HttpService,
    private activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private imageCompress: NgxImageCompressService
  ) {

    this.genreDropdown = genreList;

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'text',
    };

    this.addMovieForm = this.getMovieForm()
  }

  async ngOnInit() {
    // get cast from backend
    let cast = await this.http.getAddMovieDependency()
    this.castDropdown = [{ id: 0, text: "Others" }]
    this.castDropdown.push(...cast)


  }

  onItemSelect(item: any) {
    if (item.id == 0) {
      this.others = true
    }
  }

  onItemDeSelect(item: any) {
    if (item.id == 0) {
      this.others = false
    }
  }

  async addMovie() {
    let movie = this.getMovieFromForm()
    try {
      if (this.addMovieForm.valid) {
        let newMovie = await this.http.postAddMovie(movie)
        newMovie.posterImg = movie.image
        this.activeModal.close(newMovie);
      }
    } catch { }
  }

  cancel(): void {
    this.activeModal.dismiss('cancel');
  }

  getMovieForm() {
    return this.fb.group({
      name: ['', Validators.required],
      directorName: ['', [Validators.required]],
      cast: [],
      others: [],
      genre: [],
      image: ['']
    });
  }

  getMovieFromForm(): AddMovie {
    let selectedCast: dropdown[] = this.addMovieForm.value.cast
    let selectedGenre: dropdown[] = this.addMovieForm.value.genre
    console.log(this.addMovieForm.value)
    let otherCast: string = this.addMovieForm.value.others

    let cast = selectedCast?.filter(val => val.id > 0).map(val => { return val.text })
    let genre = selectedGenre?.map(val => val.id)
    if (otherCast) {
      cast.push(...otherCast.split(',').map(val => val.trim()))
    }
    return {
      name: this.addMovieForm.value.name,
      directorName: this.addMovieForm.value.directorName,
      cast: cast,
      genre: genre,
      image: this.addMovieForm.value.image
    }
  }

  onImgUpload() {
    this.imageCompress.uploadFile().then(({ image, orientation }) => {
      this.imageCompress.compressFile(image, orientation, 50, 50).then(
        result => {
          this.imagePath = result;
          this.imagedata = result;
        }
      );
    });
  }



}
