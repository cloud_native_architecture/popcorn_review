package moviemethods

import (
	"context"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/grpcclient/movieservice"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/tokenmanager"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/models"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/movie"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// AddReview ...
func AddReview(params movie.PostReviewParams) middleware.Responder {
	id, err := tokenmanager.GetUserID(params.HTTPRequest)

	resp, err := movieservice.AddReview(context.Background(), &services.AddReviewRequest{
		Rating:   params.Data.Rating,
		Comments: params.Data.Comments,
		MovieId:  params.Data.MovieID,
		UserId:   id,
	})

	if err != nil {
		return movie.NewPostReviewBadRequest().WithPayload(err.Error())
	}
	reply := &models.AddReviewResp{
		ID:        resp.Id,
		CreatedAt: strfmt.DateTime(resp.CreatedAt.AsTime()),
	}

	return movie.NewPostReviewOK().WithPayload(reply)
}
