package movieservice

import (
	"context"
	"time"

	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/grpcclient"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// AddMovieDep ...
func AddMovieDep(ctx context.Context, req *services.AddMovieDepRequest) (*services.AddMovieDepResponse, error) {
	movieConn, err := grpcclient.ConnectMovieService()
	defer movieConn.Close()
	if err != nil {
		return nil, err
	}

	movieServer := services.NewMovieServiceClient(movieConn)

	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	resp, err := movieServer.AddMovieDep(ctx, req)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
