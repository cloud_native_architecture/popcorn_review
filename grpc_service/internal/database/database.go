package database

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var (
	db *DB
)

// DB used to represent the Database layer and all DB connections
type DB struct {
	postgres *gorm.DB
}

// Init initializes the Database
func Init(conn string) {
	if db == nil {
		db = &DB{}
		retryCount := 0
		for connect(conn) != nil {
			retryCount++
			if retryCount > 10 {
				log.Fatal("Failed to connec to database")
			}
			time.Sleep(10 * time.Second)
		}
		db.postgres.DB()
	}
}

func connect(conn string) error {
	var err error
	db.postgres, err = gorm.Open(postgres.Open(conn), &gorm.Config{})
	db.postgres.Config.Logger = logger.Default.LogMode(logger.Info)
	if err != nil {
		return err
	}

	sqlDB, err := db.postgres.DB()
	if err != nil {
		return err
	}

	sqlDB.SetMaxOpenConns(config.GetMaxOpenConnections())
	sqlDB.SetMaxIdleConns(config.GetMaxIdleConnections())

	return nil
}

// CreateTestDB Creates a test DB with the existing template
func CreateTestDB(dbName string) {
	if db != nil {
		Close()
	}
	Init(config.GetPostgresDBString())
	defer Close()

	db, err := db.postgres.DB()
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(fmt.Sprintf("CREATE DATABASE %s_test WITH TEMPLATE %s_template;", dbName, config.GetDBName()))
	if err != nil {
		log.Fatal(err)
	}

}

// DropTestDB Deletes the Test DB
func DropTestDB(dbName string) {
	log.Println("Drop Test DB")
	if db != nil {
		Close()
	}
	Init(config.GetPostgresDBString())
	defer Close()

	res := db.postgres.Exec(fmt.Sprintf("DROP DATABASE %s_test ", dbName))
	if res.Error != nil {
		log.Fatal("drop failed", res.Error)
	}

}

// Close Closes the DB connections
func Close() {
	sqlDB, err := db.postgres.DB()
	if err != nil {
		log.Printf("[ERROR] DB close: %s \n", err)
		return
	}
	sqlDB.Close()
	db = nil
}
