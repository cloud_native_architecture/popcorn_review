module gitlab.com/cloud_native_architecture/popcorn_review/grpc_service

go 1.16

require (
	github.com/golang/protobuf v1.5.1
	github.com/stretchr/testify v1.5.1
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	google.golang.org/grpc v1.36.1
	google.golang.org/protobuf v1.26.0
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.4
)
