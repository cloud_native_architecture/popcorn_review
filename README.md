# Popcorn Review
## A basic movie review platform


Popcorn review is a basic platform where users can add new movies and add reviews to those movies.


## Features

- Basic Authentication for the users
- Add a new Movie
- Add a review to a movie (one review per user per movie )
- Get the Average ratings for all the movies at a glance

## Requirements

Popcorn Review uses a number of open source projects to work properly:

- [Angular] - Single Paged Application for the front end
- [API Gateway] - A HTTP gateway for the front end to communicate with the services
- [GRPC services] - micro-services each running on different containers which communicate with the API Gateway and service to service communication is done by GRPC
- [Database] - We used PostgreSQL db for data storage
- [Docker] - Used to deploy all the applications on the containers

| Software | version |
| ------ | ------ |
| Angular | 11.2.3 |
| Go Lang | 1.16.2 |
| Docker | 20.10.5 |
| PostgreSQL | 13.2 |
|Open API (Swagger) |  |
| Docker Compose | 1.28.6 |
| protoc | 3.13.0 |
| swagger-codegen | 3.0.25 |

## Architecture
![Project Architecture](/docs/architecture.png)


## Running

```sh
cd popcorn_review
sudo docker-compose up
```

