package moviemethods

import (
	"context"

	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/grpcclient/movieservice"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/dep"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/movie"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// DeleteMovie ...
func DeleteMovie(params movie.DeleteMovieIDParams) middleware.Responder {

	resp, err := movieservice.DeleteMovie(context.Background(), &services.DeleteMovieRequest{
		Id: int32(params.ID),
	})

	if err != nil {
		return dep.NewGetDepAddmovieBadRequest().WithPayload(err.Error())
	}
	if !resp.Status {
		return dep.NewGetDepAddmovieBadRequest().WithPayload("failed to delete movie")
	}

	return movie.NewDeleteMovieIDOK().WithPayload("Deleted movie")
}
