import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app/app.component';
import { LoginModule } from './login/login.module';
import { HeaderComponent } from './header/header.component';
import { PortalModule } from './portal/portal.module';
import { HttpClientModule } from '@angular/common/http';
import { httpInterceptorProviders } from './service/http-interceptor.service';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxImageCompressService } from 'ngx-image-compress';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    HttpClientModule,
    NgbModule,
    NgMultiSelectDropDownModule
  ],
  providers: [
    httpInterceptorProviders,
    NgxImageCompressService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
