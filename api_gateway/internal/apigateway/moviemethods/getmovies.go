package moviemethods

import (
	"context"

	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/grpcclient/movieservice"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/models"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/movie"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// GetMovies ...
func GetMovies(params movie.GetMoviesParams) middleware.Responder {

	response, err := movieservice.Movies(context.Background(), &services.GetMoviesRequest{})

	if err != nil {
		return movie.NewGetMoviesBadRequest().WithPayload(err.Error())
	}

	movies := make(models.MoviesList, 0)
	for _, movie := range response.Movies {
		movies = append(movies, &models.MoviesListItems0{
			ID:           int64(movie.Id),
			AvgReview:    movie.AvgReview,
			DirectorName: movie.DirectorName,
			Name:         movie.Name,
			PosterImg:    movie.PosterImg,
		})
	}

	return movie.NewGetMoviesOK().WithPayload(movies)
}
