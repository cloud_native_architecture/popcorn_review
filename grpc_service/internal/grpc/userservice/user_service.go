package userservice

import "gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"

// Server ...
type Server struct {
	services.UnimplementedUserServiceServer
}
