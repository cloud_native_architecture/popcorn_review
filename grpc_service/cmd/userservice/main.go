package main

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/config"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/database"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/grpc/userservice"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
	"google.golang.org/grpc"
)

func main() {
	config.Init()
	// Start listening on the port
	port := config.GetUserServicePort()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	log.Printf("API Service running on port: %d \n", port)

	s := grpc.NewServer()

	// Initialize the Database
	database.Init(config.GetPostgresDBString())
	defer database.Close()

	// Start the Server
	services.RegisterUserServiceServer(s, &userservice.Server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
