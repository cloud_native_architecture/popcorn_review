interface User extends Indexable, Deletable {
    name: string;
    email: string;
    password?: string
}

