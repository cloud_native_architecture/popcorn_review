import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/service/http.service';
import { StateService } from 'src/app/service/state.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  signupForm: FormGroup;

  constructor(
    private router: Router,
    private http: HttpService,
    private state: StateService,
    private fb: FormBuilder,
    private toastr: ToastrService) {
    this.loginForm = this.getLoginForm();
    this.signupForm = this.getSignUpForm();
  }

  ngOnInit(): void { }

  getLoginForm(): FormGroup {
    return this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  getSignUpForm(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  async signup(): Promise<void> {
    if (this.signupForm.valid) {
      try {
        let value: User = this.signupForm.value;
        let user = await this.http.postSignup(value);
        this.state.login(user);
        this.router.navigateByUrl("/app");
        this.toastr.success('', 'Login Successful');
      } catch (err) {
        console.log(err);
        this.toastr.error('Failed to add a new user', 'Error');
      }
    } else {
      this.toastr.error('Invalid Entry', 'Login Error');
    }
  }

  async login(): Promise<void> {
    if (this.loginForm.valid) {
      try {
        let value: { email: string, password: string } = this.loginForm.value;
        let user = await this.http.loginUser(value);
        this.state.login(user);
        this.router.navigateByUrl("/app");
        this.toastr.success('', 'Login Successful');
      } catch (err) {
        console.log(err);
        this.toastr.error('Failed to login User', 'Login Error');
      }
    } else {
      this.toastr.error('Invalid Entry', 'Login Error');
    }
  }
}
