package testutils

import (
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/config"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/database"
)

// Setup ...
func Setup(dbName string) func() {
	config.Init()
	// Create TestDB
	database.CreateTestDB(dbName)

	// Connect Test DB
	database.Init(config.GetPostgresTestDBString(dbName))
	return func() {
		// Close TestDB
		database.Close()
		// DROP test DB
		database.DropTestDB(dbName)
	}
}

// DummyUser ...
func DummyUser() *database.User {
	return &database.User{
		Name:     "Sai Ananth Vishwanatha",
		Email:    "svishwa2@uncc.edu",
		Password: "svishwa2",
	}
}

// DummyMovie ...
func DummyMovie(user *database.User) *database.Movie {
	movie := &database.Movie{
		Name:         "Test Movie",
		DirectorName: "Test Director",
		CreatedByID:  uint(user.ID),
		UpdatedByID:  uint(user.ID),
		Casts:        make([]database.Cast, 0),
		Genres:       make([]database.Genre, 0),
		PosterImg:    "",
	}
	testCast := [2]string{"Test Cast 1", "Test Cast 2"}
	testGenre := [2]int{1, 2}
	for _, cast := range testCast {
		movie.Casts = append(movie.Casts, database.Cast{
			Name:        cast,
			CreatedByID: uint(user.ID),
			UpdatedByID: uint(user.ID),
		})
	}

	for _, genre := range testGenre {
		movie.Genres = append(movie.Genres, database.Genre{
			ID: uint(genre),
		})
	}

	return movie
}
