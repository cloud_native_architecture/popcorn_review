
interface Indexable {
    id?: number;
}

interface Creatable {
    created_by?: number;
    createdAt?: Date | string;
}

interface Updatable {
    updated_by?: number;
    updated_at?: Date | string;
}

interface Deletable {
    is_deleted?: boolean;
}