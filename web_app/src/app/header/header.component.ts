import { Component, OnInit } from '@angular/core';
import { StateService } from '../service/state.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  username: string = ""
  constructor(private state: StateService) {
  }

  ngOnInit(): void {
    this.username = this.state.userName
  }

  logout(): void {
    this.state.logout();
  }

}
