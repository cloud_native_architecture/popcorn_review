package movieservice

import (
	"context"

	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/database"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// AddReview ...
func (s *Server) AddReview(ctx context.Context, req *services.AddReviewRequest) (*services.AddReviewResponse, error) {

	review := &database.Review{
		MovieID:     uint(req.GetMovieId()),
		Comments:    req.GetComments(),
		Rating:      int(req.GetRating()),
		CreatedByID: uint(req.GetUserId()),
		UpdatedByID: uint(req.GetUserId()),
	}

	err := review.Add()
	if err != nil {
		return nil, err
	}

	reply := &services.AddReviewResponse{
		Id:        int32(review.ID),
		CreatedAt: timestamppb.New(review.CreatedAt),
	}

	return reply, nil
}
