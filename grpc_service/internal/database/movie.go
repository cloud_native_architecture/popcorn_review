package database

import (
	"errors"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

// Movie struct used to hold data from the Movies table
type Movie struct {
	gorm.Model
	Name         string
	DirectorName string
	CreatedByID  uint
	UpdatedByID  uint
	PosterImg    string

	Casts  []Cast  `gorm:"many2many:movie_cast;"`
	Genres []Genre `gorm:"many2many:movie_genre;"`

	AvgRating float32 `gorm:"->"`
}

// Genre holds the data for genre table
type Genre struct {
	ID   uint
	Name string
}

// Add Adds the movie to the database
func (movie *Movie) Add() error {
	// check if already exists
	tempMovie := &Movie{}
	err := tempMovie.GetByName(movie.Name)

	if err == nil {
		return status.Error(codes.AlreadyExists, "movie already exists")
	}
	if !errors.Is(err, gorm.ErrRecordNotFound) {
		return status.Error(codes.Internal, err.Error())
	}
	// add to database
	err = (&Cast{}).AddAll(movie.Casts)
	if err != nil {
		return err
	}
	err = db.postgres.Omit("Genres.*,Casts.*").Create(&movie).Error
	if err != nil {
		return err
	}
	return nil
}

// Get Gets the respective movie with that ID
func (movie *Movie) Get() error {

	err := db.postgres.
		Select("movies.*,avg(reviews.rating) as avg_rating").
		Joins("LEFT JOIN reviews on reviews.movie_id = movies.id").
		Group("movies.id").
		Preload("Casts").Preload("Genres").First(movie).Error
	if err != nil {
		return err
	}
	return nil
}

// GetAll gets all the available movies in the database
func (movie *Movie) GetAll() ([]Movie, error) {
	var movies []Movie
	err := db.postgres.
		Select("movies.*,avg(reviews.rating) as avg_rating").
		Joins("LEFT JOIN reviews on reviews.movie_id = movies.id").
		Group("movies.id").
		Order("movies.created_at desc").
		Find(&movies).Error
	if err != nil {
		return nil, err
	}

	return movies, nil
}

// GetByName gets a movie with its name
func (movie *Movie) GetByName(name string) error {

	err := db.postgres.Where("name = ?", name).Take(movie).Error
	if err != nil {
		return err
	}
	return nil
}

// Delete deleats the movie from the database
func (movie *Movie) Delete() error {
	// check if we can delete a movie
	if movie.ID == 0 {
		return status.Error(codes.InvalidArgument, "invalid movie ID")
	}
	// Delete all related reviews
	err := movie.DeleteAllReviews()
	if err != nil {
		return err
	}
	// TODO: Delete related mappings in genre and cast tables

	result := db.postgres.Delete(movie)
	if result.Error != nil {
		return result.Error
	}
	if result.RowsAffected == 0 {
		return status.Error(codes.InvalidArgument, "Movie couldnot be deleted")
	}
	return nil
}

// DeleteAllReviews deletes all the reviews for that movie
func (movie *Movie) DeleteAllReviews() error {
	if movie.ID == 0 {
		return status.Error(codes.InvalidArgument, "invalid movie ID")
	}

	result := db.postgres.Where("movie_id = ?", movie.ID).Delete(&Review{})

	if result.Error != nil {
		return result.Error
	}
	return nil
}
