import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PortalRoutingModule } from './portal-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ViewMovieComponent } from './movie/view-movie/view-movie.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AddMovieComponent } from './movie/add-movie/add-movie.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AddReviewComponent } from './movie/add-review/add-review.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxImageCompressService } from 'ngx-image-compress';


@NgModule({
  declarations: [DashboardComponent, ViewMovieComponent, AddMovieComponent, AddReviewComponent,],
  imports: [
    CommonModule,
    PortalRoutingModule,
    NgbModule,
    NgMultiSelectDropDownModule,
    ReactiveFormsModule
  ],
  providers: [NgxImageCompressService]
})
export class PortalModule { }
