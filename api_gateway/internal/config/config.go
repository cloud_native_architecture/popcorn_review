package config

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"time"
)

// Config ...
type Config struct {
	Port int `json:"port"`
	Nats struct {
		URL   string `json:"url"`
		Token string `json:"token"`
	} `json:"nats"`
	Grpc struct {
		UserService  string `json:"userService"`
		MovieService string `json:"movieService"`
	}
	Cors struct {
		AllowedHeaders   []string `json:"allowedHeaders"`
		AllowedOrigins   []string `json:"allowedOrigins"`
		AllowedMethods   []string `json:"allowedMethods"`
		ExposedHeaders   []string `json:"exposedHeaders"`
		AllowCredentials bool     `json:"allowCredentials"`
		MaxAge           int      `json:"maxAge"`
	} `json:"cors"`
	JWT struct {
		Alg                   string `json:"alg"`
		HMACKey               string `json:"hmacKey"`
		RefreshTokenValidTime int    `json:"refreshTokenValidTime"` //in hrs
		AuthTokenValidTime    int    `json:"authTokenValidTime"`    // in mins
	} `json:"jwt"`
	Prod bool `json:"prod"`
}

var (
	conf = &Config{}
)

func init() {
	pathPtr := flag.String("config", "config.json", "Set the path for the server config file")
	flag.Parse()
	jsonFile, err := os.ReadFile(*pathPtr)

	if err != nil {
		fmt.Println(err)
	}
	json.Unmarshal(jsonFile, conf)
}

// GetAuthTokenValidTime ...
func GetAuthTokenValidTime() time.Duration {
	return time.Duration(conf.JWT.AuthTokenValidTime * int(time.Minute))
}

// GetRefreshTokenValidTime ...
func GetRefreshTokenValidTime() time.Duration {
	return time.Duration(conf.JWT.RefreshTokenValidTime * int(time.Minute))
}

// GetHMACKey ...
func GetHMACKey() string {
	return conf.JWT.HMACKey
}

// GetJWTAlg ...
func GetJWTAlg() string {
	return conf.JWT.Alg
}

// GetServerPort ...
func GetServerPort() int {
	return conf.Port
}

// GetNatsURL ...
func GetNatsURL() string {
	return conf.Nats.URL
}

// GetNatsToken ...
func GetNatsToken() string {
	return conf.Nats.Token
}

// GetAllowedHeaders ...
func GetAllowedHeaders() []string {
	return conf.Cors.AllowedHeaders
}

// GetAllowedOrigins ...
func GetAllowedOrigins() []string {
	return conf.Cors.AllowedOrigins
}

// GetAllowedMethods ...
func GetAllowedMethods() []string {
	return conf.Cors.AllowedMethods
}

// GetExposedHeaders ...
func GetExposedHeaders() []string {
	return conf.Cors.ExposedHeaders
}

// GetAllowCredentials ...
func GetAllowCredentials() bool {
	return conf.Cors.AllowCredentials
}

// GetMaxAge ...
func GetMaxAge() int {
	return conf.Cors.MaxAge
}

// IsProd ...
func IsProd() bool {
	return conf.Prod
}

// GetGrpcUserService ...
func GetGrpcUserService() string {
	return conf.Grpc.UserService
}

// GetGrpcMovieService ...
func GetGrpcMovieService() string {
	return conf.Grpc.MovieService
}
