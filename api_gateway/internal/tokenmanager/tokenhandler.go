package tokenmanager

import (
	"log"
	"net/http"
	"strconv"

	"github.com/adam-hanna/jwt-auth/jwt"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/config"
)

var (
	// TokenHandler ...
	TokenHandler jwt.Auth
)

func init() {
	authErr := jwt.New(&TokenHandler, jwt.Options{
		SigningMethodString:   config.GetJWTAlg(),
		HMACKey:               []byte(config.GetHMACKey()),
		RefreshTokenValidTime: config.GetRefreshTokenValidTime(),
		AuthTokenValidTime:    config.GetAuthTokenValidTime(),
		Debug:                 !config.IsProd(),
		IsDevEnv:              true,
	})
	log.Printf("in dev env %v", config.IsProd())
	if authErr != nil {
		log.Println("Error initializing the JWT's!")
		log.Println(config.GetJWTAlg())
		log.Fatal(authErr)
	}
}

// GetUserID Gets the user id from claims
func GetUserID(r *http.Request) (int32, error) {
	claims, _ := TokenHandler.GrabTokenClaims(r)
	id, err := strconv.Atoi(claims.StandardClaims.Id)
	if err != nil {
		return 0, nil
	}
	return int32(id), nil
}
