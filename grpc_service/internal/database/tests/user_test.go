package database_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/database"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/testutils"
)

func TestAdd(t *testing.T) {
	defer testutils.Setup("db")()

	user := testutils.DummyUser()
	// Adding a new User should Pass
	err := user.Add()
	assert.NoError(t, err)

	// Adding the same user should cause the error
	err = user.Add()
	assert.Error(t, err)
	assert.NotEqual(t, user.ID, 0, "User ID must not be 0")
}

func TestGet(t *testing.T) {
	defer testutils.Setup("db")()

	user := testutils.DummyUser()
	// Adding a new User should Pass
	err := user.Add()
	assert.NoError(t, err)

	user = &database.User{
		Name: "Sai Ananth Vishwanatha",
	}
	err = user.Get()
	assert.NoError(t, err, "Failed to get user")
	assert.NotEqual(t, user.ID, 0, "User ID must not be 0")
}

func TestGetByEmail(t *testing.T) {
	defer testutils.Setup("db")()

	user := testutils.DummyUser()
	// Adding a new User should Pass
	err := user.Add()
	assert.NoError(t, err, "Failed to Add user")

	// Get User by email
	user = &database.User{
		Email: "svishwa2@uncc.edu",
	}
	err = user.GetByEmail()
	assert.NoError(t, err, "Failed to get user")
	assert.NotEqual(t, user.ID, 0, "User ID must not be 0")
}

func TestUpdate(t *testing.T) {
	defer testutils.Setup("db")()

	user := testutils.DummyUser()
	// Adding a new User should Pass
	err := user.Add()
	assert.NoError(t, err, "Failed to Add user")

	newUser := &database.User{
		Email: "saiananth@gmail.com",
	}
	err = user.Update(*newUser)
	assert.NoError(t, err, "Failed to Update user")
	assert.Equal(t, user.Email, newUser.Email, "user should be updated with newUser Email ")
}

func TestDelete(t *testing.T) {
	defer testutils.Setup("db")()

	user := testutils.DummyUser()
	// Adding a new User should Pass
	err := user.Add()
	assert.NoError(t, err, "Failed to Add user")

	err = user.Delete()
	assert.NoError(t, err, "Failed to Delete user")
}
