package movieservice

import (
	"context"

	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/database"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// AddMovie ...
func (s *Server) AddMovie(ctx context.Context, req *services.AddMovieRequest) (*services.AddMovieResponse, error) {

	movie := &database.Movie{
		Name:         req.GetName(),
		DirectorName: req.GetDirectorName(),
		CreatedByID:  uint(req.GetUserId()),
		UpdatedByID:  uint(req.GetUserId()),
		Casts:        make([]database.Cast, 0),
		Genres:       make([]database.Genre, 0),
		PosterImg:    req.GetPosterImg(),
	}

	for _, cast := range req.GetCast() {
		movie.Casts = append(movie.Casts, database.Cast{
			Name:        cast,
			CreatedByID: uint(req.GetUserId()),
			UpdatedByID: uint(req.GetUserId()),
		})
	}

	for _, genre := range req.GetGenre() {
		movie.Genres = append(movie.Genres, database.Genre{
			ID: uint(genre),
		})
	}

	err := movie.Add()
	if err != nil {
		return nil, err
	}

	reply := &services.AddMovieResponse{
		Id:           int32(movie.ID),
		Name:         movie.Name,
		DirectorName: movie.DirectorName,
	}

	return reply, nil
}
