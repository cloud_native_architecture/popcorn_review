package usermethods

import (
	"context"
	"strconv"

	"github.com/adam-hanna/jwt-auth/jwt"
	"github.com/go-openapi/swag"

	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/grpcclient/userservice"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/security"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/models"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/user"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// Login method endpoint
func Login(params user.GetLoginParams) middleware.Responder {
	email, pwd, err := security.GetEmailAndPwd(params.Authorization)

	if err != nil {
		return user.NewGetLoginBadRequest().WithPayload(err.Error())
	}

	response, err := userservice.Login(context.Background(), &services.LogInRequest{
		Email:    email,
		Password: pwd,
	})

	if err != nil {
		return user.NewGetLoginBadRequest().WithPayload(err.Error())
	}

	resp := user.NewGetLoginOK()
	claims := jwt.ClaimsType{}
	claims.StandardClaims.Id = strconv.Itoa(int(response.Id))

	return security.NewTokenResponder(&claims, resp.WithPayload(&models.LoginResp{
		Email: swag.String(response.Email),
		ID:    swag.Int64(int64(response.Id)),
		Name:  swag.String(response.Name),
	}))
}
