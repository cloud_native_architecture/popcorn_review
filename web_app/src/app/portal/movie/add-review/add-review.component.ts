import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-review',
  templateUrl: './add-review.component.html',
  styleUrls: ['./add-review.component.scss']
})
export class AddReviewComponent implements OnInit {
  reviewForm: FormGroup
  constructor(
    private fb: FormBuilder,
    private activeModal: NgbActiveModal,
  ) {
    this.reviewForm = this.getReviewForm()
  }
  ngOnInit(): void { }

  getReviewForm(): FormGroup {
    return this.fb.group({
      rating: [0, Validators.required],
      comments: ['', [Validators.required]],
    })
  }

  addReview() {
    if (this.reviewForm.valid) {
      this.activeModal.close(this.reviewForm.value)
    }
  }

  cancel() {
    this.activeModal.dismiss('cancel');
  }

}
