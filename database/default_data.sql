-- Default Data for Genre

INSERT INTO public.genres VALUES (1, 'Comedy');
INSERT INTO public.genres VALUES (2, 'Drama');
INSERT INTO public.genres VALUES (3, 'Fantasy');
INSERT INTO public.genres VALUES (4, 'Horror');
INSERT INTO public.genres VALUES (5, 'Mystery');
INSERT INTO public.genres VALUES (6, 'Romance');
INSERT INTO public.genres VALUES (7, 'Thriller');
INSERT INTO public.genres VALUES (8, 'Western');
INSERT INTO public.genres VALUES (9, 'Action');