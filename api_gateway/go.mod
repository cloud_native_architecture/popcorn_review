module gitlab.com/cloud_native_architecture/popcorn_review/api_gateway

go 1.16

require (
	github.com/adam-hanna/jwt-auth/jwt v0.0.0-20201030162425-311bbbabba65
	github.com/go-openapi/errors v0.20.0
	github.com/go-openapi/loads v0.20.2
	github.com/go-openapi/runtime v0.19.27
	github.com/go-openapi/spec v0.20.3
	github.com/go-openapi/strfmt v0.20.0
	github.com/go-openapi/swag v0.19.14
	github.com/go-openapi/validate v0.20.2
	github.com/jessevdk/go-flags v1.5.0
	github.com/nats-io/nats-server/v2 v2.2.0 // indirect
	github.com/nats-io/nats.go v1.10.1-0.20210228004050-ed743748acac
	github.com/rs/cors v1.7.0
	gitlab.com/cloud_native_architecture/popcorn_review/grpc_service v0.0.0-20210423121313-9c0e872680d2
	golang.org/x/net v0.0.0-20210330142815-c8897c278d10
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/grpc v1.36.1
)
