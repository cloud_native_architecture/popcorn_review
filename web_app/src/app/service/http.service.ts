import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { endpoints } from '../models/data/endpoints';
import { StateService } from './state.service';


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  base: string;
  constructor(private http: HttpClient) {
    if (environment.production && environment.apiEndpoint === "") {
      this.base = "http://" + window.location.hostname + ":8081"
    } else {
      this.base = environment.apiEndpoint;
    }
    console.log("connected to " + this.base)
  }

  getUrl(endpoint: string) {
    return this.base + endpoint;
  }

  postSignup(data: User): Promise<User> {
    return this.http.post<User>(this.getUrl(endpoints.signup), data).toPromise();
  }

  loginUser(data: { email: string, password: string }): Promise<User> {
    let header = new HttpHeaders().set("Authorization", "Basic " + btoa(data.email + ":" + data.password))
    return this.http.get<User>(this.getUrl(endpoints.login), { headers: header }).toPromise();
  }

  getMovieList(): Promise<Movie[]> {
    return this.http.get<Movie[]>(this.getUrl(endpoints.getMovieList)).toPromise();
  }


  getAddMovieDependency(): Promise<dropdown[]> {
    return this.http.get<dropdown[]>(this.getUrl(endpoints.addMovieDep)).toPromise();
  }

  postAddMovie(movie: AddMovie): Promise<Movie> {
    return this.http.post<AddMovie>(this.getUrl(endpoints.addMovie), movie).toPromise();
  }

  getMovie(id: number): Promise<MovieInfo> {
    return this.http.get<MovieInfo>(this.getUrl(endpoints.getMovie) + "/" + id).toPromise();
  }

  deleteMovie(id: number): Promise<string> {
    return this.http.delete<string>(this.getUrl(endpoints.getMovie) + "/" + id).toPromise();
  }

  postAddReview(review: Review): Promise<Review> {
    return this.http.post<Review>(this.getUrl(endpoints.postReview), review).toPromise();
  }
}
