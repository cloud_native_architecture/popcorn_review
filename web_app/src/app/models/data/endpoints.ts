export let endpoints = {
    login: "/login",
    signup: "/signup",
    getMovieList: "/movies",
    getMovie: "/movie",
    addMovieDep: "/dep/addmovie",
    addMovie: "/movie",
    postReview: "/review"
}