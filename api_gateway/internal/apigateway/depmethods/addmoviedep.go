package depmethods

import (
	"context"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/grpcclient/movieservice"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/models"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/dep"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// AddMovieDep ...
func AddMovieDep(params dep.GetDepAddmovieParams) middleware.Responder {

	response, err := movieservice.AddMovieDep(context.Background(), &services.AddMovieDepRequest{})

	if err != nil {
		return dep.NewGetDepAddmovieBadRequest().WithPayload(err.Error())
	}

	casts := make(models.AddMovieDep, 0)
	for _, cast := range response.Casts {
		casts = append(casts, &models.AddMovieDepItems0{
			ID:   swag.Int64(int64(cast.Id)),
			Text: swag.String(cast.Text),
		})
	}

	return dep.NewGetDepAddmovieOK().WithPayload(casts)
}
