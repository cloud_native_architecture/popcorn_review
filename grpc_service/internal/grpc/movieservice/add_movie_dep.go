package movieservice

import (
	"context"

	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/database"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// AddMovieDep ...
func (s *Server) AddMovieDep(ctx context.Context, req *services.AddMovieDepRequest) (*services.AddMovieDepResponse, error) {
	var casts []database.Cast
	casts, err := (&database.Cast{}).GetAll()
	if err != nil {
		return nil, err
	}

	respCasts := make([]*services.AddMovieDepResponse_Cast, 0)

	for _, cast := range casts {
		respCasts = append(respCasts, &services.AddMovieDepResponse_Cast{
			Id:   int32(cast.ID),
			Text: cast.Name,
		})
	}

	reply := &services.AddMovieDepResponse{
		Casts: respCasts,
	}
	return reply, nil
}
