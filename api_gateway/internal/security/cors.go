package security

import (
	"net/http"

	"github.com/rs/cors"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/config"
)

// SetupCORSMiddleware ...
func SetupCORSMiddleware(handler http.Handler) http.Handler {
	corsHandler := cors.New(cors.Options{
		Debug:            !config.IsProd(),
		AllowedHeaders:   config.GetAllowedHeaders(),
		AllowedOrigins:   config.GetAllowedOrigins(), //[]string{"http://localhost:4200", "http://localhost:81", "http://web:81"},
		AllowedMethods:   config.GetAllowedMethods(),
		AllowCredentials: config.GetAllowCredentials(),
		ExposedHeaders:   config.GetExposedHeaders(), // []string{"x-csrf-token"},
		MaxAge:           config.GetMaxAge(),
	})
	return corsHandler.Handler(handler)
}
