package moviemethods

import (
	"context"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/grpcclient/movieservice"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/tokenmanager"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/models"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/dep"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/movie"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services"
)

// AddMovie ...
func AddMovie(params movie.PostMovieParams) middleware.Responder {
	id, err := tokenmanager.GetUserID(params.HTTPRequest)

	resp, err := movieservice.AddMovie(context.Background(), &services.AddMovieRequest{
		Name:         *params.Data.Name,
		DirectorName: *params.Data.DirectorName,
		Genre:        params.Data.Genre,
		Cast:         params.Data.Cast,
		UserId:       id,
		PosterImg:    *params.Data.Image,
	})

	if err != nil {
		return dep.NewGetDepAddmovieBadRequest().WithPayload(err.Error())
	}
	reply := &models.AddMovieResp{
		DirectorName: swag.String(resp.GetDirectorName()),
		ID:           swag.Int64(int64(resp.GetId())),
		Name:         swag.String(resp.GetName()),
	}

	return movie.NewPostMovieOK().WithPayload(reply)
}
