package apigateway

import (
	"log"

	"github.com/go-openapi/loads"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/apigateway/depmethods"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/apigateway/moviemethods"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/apigateway/usermethods"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/config"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/security"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/tokenmanager"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/dep"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/movie"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/user"
)

var (
	server *restapi.Server
)

func init() {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.Fatalln(err)
	}
	api := operations.NewAPIGatewayAPI(swaggerSpec)

	api.UserGetLoginHandler = user.GetLoginHandlerFunc(usermethods.Login)
	api.UserPostSignupHandler = user.PostSignupHandlerFunc(usermethods.Signup)
	api.MovieGetMoviesHandler = movie.GetMoviesHandlerFunc(moviemethods.GetMovies)
	api.DepGetDepAddmovieHandler = dep.GetDepAddmovieHandlerFunc(depmethods.AddMovieDep)
	api.MoviePostMovieHandler = movie.PostMovieHandlerFunc(moviemethods.AddMovie)
	api.MovieGetMovieIDHandler = movie.GetMovieIDHandlerFunc(moviemethods.GetMovie)
	api.MoviePostReviewHandler = movie.PostReviewHandlerFunc(moviemethods.AddReview)
	api.MovieDeleteMovieIDHandler = movie.DeleteMovieIDHandlerFunc(moviemethods.DeleteMovie)

	api.AddMiddlewareFor("GET", "/movies", tokenmanager.TokenHandler.Handler)
	api.AddMiddlewareFor("GET", "/dep/addmovie", tokenmanager.TokenHandler.Handler)
	api.AddMiddlewareFor("POST", "/movie", tokenmanager.TokenHandler.Handler)
	api.AddMiddlewareFor("GET", "/movie/{id}", tokenmanager.TokenHandler.Handler)
	api.AddMiddlewareFor("POST", "/review", tokenmanager.TokenHandler.Handler)
	api.AddMiddlewareFor("DELETE", "/movie/{id}", tokenmanager.TokenHandler.Handler)

	server = restapi.NewServer(api)
	server.ConfigureAPI()

	server.SetHandler(security.SetupCORSMiddleware(server.GetHandler()))
	server.Port = config.GetServerPort()
}

// GetServer ...
func GetServer() *restapi.Server {
	return server
}

// Close ...
func Close() {
	if err := server.Shutdown(); err != nil {
		// error handle
		log.Fatalln(err)
	}
}
