// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/dep"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/movie"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/pkg/swagger/server/restapi/operations/user"
)

//go:generate swagger generate server --target ../../server --name APIGateway --spec ../../swagger.yaml --principal interface{} --exclude-main

func configureFlags(api *operations.APIGatewayAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.APIGatewayAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	// api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	if api.MovieDeleteMovieIDHandler == nil {
		api.MovieDeleteMovieIDHandler = movie.DeleteMovieIDHandlerFunc(func(params movie.DeleteMovieIDParams) middleware.Responder {
			return middleware.NotImplemented("operation movie.DeleteMovieID has not yet been implemented")
		})
	}
	if api.DepGetDepAddmovieHandler == nil {
		api.DepGetDepAddmovieHandler = dep.GetDepAddmovieHandlerFunc(func(params dep.GetDepAddmovieParams) middleware.Responder {
			return middleware.NotImplemented("operation dep.GetDepAddmovie has not yet been implemented")
		})
	}
	if api.UserGetLoginHandler == nil {
		api.UserGetLoginHandler = user.GetLoginHandlerFunc(func(params user.GetLoginParams) middleware.Responder {
			return middleware.NotImplemented("operation user.GetLogin has not yet been implemented")
		})
	}
	if api.MovieGetMovieIDHandler == nil {
		api.MovieGetMovieIDHandler = movie.GetMovieIDHandlerFunc(func(params movie.GetMovieIDParams) middleware.Responder {
			return middleware.NotImplemented("operation movie.GetMovieID has not yet been implemented")
		})
	}
	if api.MovieGetMoviesHandler == nil {
		api.MovieGetMoviesHandler = movie.GetMoviesHandlerFunc(func(params movie.GetMoviesParams) middleware.Responder {
			return middleware.NotImplemented("operation movie.GetMovies has not yet been implemented")
		})
	}
	if api.MoviePostMovieHandler == nil {
		api.MoviePostMovieHandler = movie.PostMovieHandlerFunc(func(params movie.PostMovieParams) middleware.Responder {
			return middleware.NotImplemented("operation movie.PostMovie has not yet been implemented")
		})
	}
	if api.MoviePostReviewHandler == nil {
		api.MoviePostReviewHandler = movie.PostReviewHandlerFunc(func(params movie.PostReviewParams) middleware.Responder {
			return middleware.NotImplemented("operation movie.PostReview has not yet been implemented")
		})
	}
	if api.UserPostSignupHandler == nil {
		api.UserPostSignupHandler = user.PostSignupHandlerFunc(func(params user.PostSignupParams) middleware.Responder {
			return middleware.NotImplemented("operation user.PostSignup has not yet been implemented")
		})
	}

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix".
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation.
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics.
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
