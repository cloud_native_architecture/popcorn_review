package config

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

// Config used to store the Configuration
type Config struct {
	Port               int    `json:"port"`
	DbHost             string `json:"dbHost"`
	DbPort             int    `json:"dbPort"`
	DbUser             string `json:"dbUser"`
	DbPwd              string `json:"dbPwd"`
	DbName             string `json:"dbName"`
	SslMode            string `json:"sslMode"`
	MaxOpenConnections int    `json:"maxOpenConnections"`
	MaxIdleConnections int    `json:"maxIdleConnections"`
	ServicePorts       struct {
		UserServicePort  int `json:"userServicePort"`
		MovieServicePort int `json:"movieServicePort"`
	} `json:"servicePorts"`
}

var (
	conf    = &Config{}
	pathPtr = flag.String("config", "config.json", "Set the path for the server config file")
)

// Init initialize the The Config File
func Init() {
	flag.Parse()
	jsonFile, err := os.ReadFile(*pathPtr)

	if err != nil {
		panic(err)
	}
	json.Unmarshal(jsonFile, conf)
}

// GetDBName Gets the DB name
func GetDBName() string {
	return conf.DbName
}

// GetPostgresTestDBString gets Test DB Conn String
func GetPostgresTestDBString(dbname string) string {
	return fmt.Sprintf("host=%s port=%d user=%s dbname=%s_test sslmode=%s password=%s", conf.DbHost, conf.DbPort, conf.DbUser,
		dbname, conf.SslMode, conf.DbPwd)
}

// GetPostgresDBString Gets DB conn String
func GetPostgresDBString() string {
	dbConn := fmt.Sprintf("host=%s port=%d user=%s dbname=%s sslmode=%s password=%s", conf.DbHost, conf.DbPort, conf.DbUser,
		conf.DbName, conf.SslMode, conf.DbPwd)
	return dbConn
}

// GetMaxOpenConnections ...
func GetMaxOpenConnections() int {
	return conf.MaxOpenConnections
}

// GetMaxIdleConnections ...
func GetMaxIdleConnections() int {
	return conf.MaxIdleConnections
}

// GetMovieServicePort ...
func GetMovieServicePort() int {
	return conf.ServicePorts.MovieServicePort
}

// GetUserServicePort ...
func GetUserServicePort() int {
	return conf.ServicePorts.UserServicePort
}
