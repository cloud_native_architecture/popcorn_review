package database_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/internal/testutils"
)

func TestAddMovie(t *testing.T) {
	defer testutils.Setup("db")()
	user := testutils.DummyUser()
	// Adding a new User should Pass
	err := user.Add()
	assert.NoError(t, err, "Failed to add User")

	// Get a Dummy Movie
	movie := testutils.DummyMovie(user)
	// Add Movie
	err = movie.Add()
	assert.NotEqual(t, movie.ID, 0, "movie ID must not be 0")
}
