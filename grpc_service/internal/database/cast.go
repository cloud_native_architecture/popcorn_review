package database

import (
	"gorm.io/gorm"
)

// Cast represents the Casts Table in the database
type Cast struct {
	gorm.Model
	Name        string
	CreatedByID uint
	UpdatedByID uint
}

// GetAll gets all the cast present in the database which are not deleted
func (cast *Cast) GetAll() ([]Cast, error) {
	var casts []Cast
	err := db.postgres.Find(&casts).Error
	if err != nil {
		return nil, err
	}

	return casts, nil
}

// AddAll Adds all the cast given
func (cast *Cast) AddAll(casts []Cast) error {
	for i := range casts {
		db.postgres.Where(Cast{Name: casts[i].Name}).FirstOrCreate(&casts[i])
	}
	return nil
}
