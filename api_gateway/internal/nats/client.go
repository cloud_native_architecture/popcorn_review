package nats

import (
	"github.com/nats-io/nats.go"
	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/config"
)

// Client ...
type Client struct {
	conn        *nats.Conn
	encodedConn *nats.EncodedConn
}

func (c *Client) connect() error {
	var err error
	c.conn, err = nats.Connect(config.GetNatsURL(), nats.Token(config.GetNatsToken()))
	if err != nil {
		return err
	}
	c.encodedConn, err = nats.NewEncodedConn(c.conn, nats.JSON_ENCODER)
	if err != nil {
		return err
	}
	return nil
}

func (c *Client) close() {
	c.conn.Close()
	c.encodedConn.Close()
}
