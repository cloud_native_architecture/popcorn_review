# github.com/PuerkitoBio/purell v1.1.1
github.com/PuerkitoBio/purell
# github.com/PuerkitoBio/urlesc v0.0.0-20170810143723-de5bf2ad4578
github.com/PuerkitoBio/urlesc
# github.com/adam-hanna/jwt-auth/jwt v0.0.0-20201030162425-311bbbabba65
## explicit
github.com/adam-hanna/jwt-auth/jwt
# github.com/adam-hanna/randomstrings v0.0.0-20160715001758-88fd7c52a2c7
github.com/adam-hanna/randomstrings
# github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef
github.com/asaskevich/govalidator
# github.com/dgrijalva/jwt-go v3.0.1-0.20160617170158-f0777076321a+incompatible
github.com/dgrijalva/jwt-go
# github.com/docker/go-units v0.4.0
github.com/docker/go-units
# github.com/go-openapi/analysis v0.20.0
github.com/go-openapi/analysis
github.com/go-openapi/analysis/internal
# github.com/go-openapi/errors v0.20.0
## explicit
github.com/go-openapi/errors
# github.com/go-openapi/jsonpointer v0.19.5
github.com/go-openapi/jsonpointer
# github.com/go-openapi/jsonreference v0.19.5
github.com/go-openapi/jsonreference
# github.com/go-openapi/loads v0.20.2
## explicit
github.com/go-openapi/loads
# github.com/go-openapi/runtime v0.19.27
## explicit
github.com/go-openapi/runtime
github.com/go-openapi/runtime/flagext
github.com/go-openapi/runtime/logger
github.com/go-openapi/runtime/middleware
github.com/go-openapi/runtime/middleware/denco
github.com/go-openapi/runtime/middleware/header
github.com/go-openapi/runtime/middleware/untyped
github.com/go-openapi/runtime/security
# github.com/go-openapi/spec v0.20.3
## explicit
github.com/go-openapi/spec
# github.com/go-openapi/strfmt v0.20.0
## explicit
github.com/go-openapi/strfmt
# github.com/go-openapi/swag v0.19.14
## explicit
github.com/go-openapi/swag
# github.com/go-openapi/validate v0.20.2
## explicit
github.com/go-openapi/validate
# github.com/go-stack/stack v1.8.0
github.com/go-stack/stack
# github.com/golang/protobuf v1.5.1
github.com/golang/protobuf/proto
github.com/golang/protobuf/ptypes
github.com/golang/protobuf/ptypes/any
github.com/golang/protobuf/ptypes/duration
github.com/golang/protobuf/ptypes/timestamp
# github.com/jessevdk/go-flags v1.5.0
## explicit
github.com/jessevdk/go-flags
# github.com/josharian/intern v1.0.0
github.com/josharian/intern
# github.com/mailru/easyjson v0.7.6
github.com/mailru/easyjson/buffer
github.com/mailru/easyjson/jlexer
github.com/mailru/easyjson/jwriter
# github.com/mitchellh/mapstructure v1.4.1
github.com/mitchellh/mapstructure
# github.com/nats-io/nats-server/v2 v2.2.0
## explicit
# github.com/nats-io/nats.go v1.10.1-0.20210228004050-ed743748acac
## explicit
github.com/nats-io/nats.go
github.com/nats-io/nats.go/encoders/builtin
github.com/nats-io/nats.go/util
# github.com/nats-io/nkeys v0.3.0
github.com/nats-io/nkeys
# github.com/nats-io/nuid v1.0.1
github.com/nats-io/nuid
# github.com/rs/cors v1.7.0
## explicit
github.com/rs/cors
# gitlab.com/cloud_native_architecture/popcorn_review/grpc_service v0.0.0-20210423121313-9c0e872680d2
## explicit
gitlab.com/cloud_native_architecture/popcorn_review/grpc_service/pkg/services
# go.mongodb.org/mongo-driver v1.4.6
go.mongodb.org/mongo-driver/bson
go.mongodb.org/mongo-driver/bson/bsoncodec
go.mongodb.org/mongo-driver/bson/bsonoptions
go.mongodb.org/mongo-driver/bson/bsonrw
go.mongodb.org/mongo-driver/bson/bsontype
go.mongodb.org/mongo-driver/bson/primitive
go.mongodb.org/mongo-driver/x/bsonx/bsoncore
# golang.org/x/crypto v0.0.0-20210314154223-e6e6c4f2bb5b
golang.org/x/crypto/ed25519
golang.org/x/crypto/ed25519/internal/edwards25519
# golang.org/x/net v0.0.0-20210330142815-c8897c278d10
## explicit
golang.org/x/net/http/httpguts
golang.org/x/net/http2
golang.org/x/net/http2/hpack
golang.org/x/net/idna
golang.org/x/net/internal/timeseries
golang.org/x/net/netutil
golang.org/x/net/trace
# golang.org/x/sys v0.0.0-20210324051608-47abb6519492
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
# golang.org/x/text v0.3.5
golang.org/x/text/secure/bidirule
golang.org/x/text/transform
golang.org/x/text/unicode/bidi
golang.org/x/text/unicode/norm
golang.org/x/text/width
# golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
## explicit
# google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013
google.golang.org/genproto/googleapis/rpc/status
# google.golang.org/grpc v1.36.1
## explicit
google.golang.org/grpc
google.golang.org/grpc/attributes
google.golang.org/grpc/backoff
google.golang.org/grpc/balancer
google.golang.org/grpc/balancer/base
google.golang.org/grpc/balancer/grpclb/state
google.golang.org/grpc/balancer/roundrobin
google.golang.org/grpc/binarylog/grpc_binarylog_v1
google.golang.org/grpc/codes
google.golang.org/grpc/connectivity
google.golang.org/grpc/credentials
google.golang.org/grpc/encoding
google.golang.org/grpc/encoding/proto
google.golang.org/grpc/grpclog
google.golang.org/grpc/internal
google.golang.org/grpc/internal/backoff
google.golang.org/grpc/internal/balancerload
google.golang.org/grpc/internal/binarylog
google.golang.org/grpc/internal/buffer
google.golang.org/grpc/internal/channelz
google.golang.org/grpc/internal/credentials
google.golang.org/grpc/internal/envconfig
google.golang.org/grpc/internal/grpclog
google.golang.org/grpc/internal/grpcrand
google.golang.org/grpc/internal/grpcsync
google.golang.org/grpc/internal/grpcutil
google.golang.org/grpc/internal/metadata
google.golang.org/grpc/internal/resolver
google.golang.org/grpc/internal/resolver/dns
google.golang.org/grpc/internal/resolver/passthrough
google.golang.org/grpc/internal/resolver/unix
google.golang.org/grpc/internal/serviceconfig
google.golang.org/grpc/internal/status
google.golang.org/grpc/internal/syscall
google.golang.org/grpc/internal/transport
google.golang.org/grpc/internal/transport/networktype
google.golang.org/grpc/keepalive
google.golang.org/grpc/metadata
google.golang.org/grpc/peer
google.golang.org/grpc/resolver
google.golang.org/grpc/serviceconfig
google.golang.org/grpc/stats
google.golang.org/grpc/status
google.golang.org/grpc/tap
# google.golang.org/protobuf v1.26.0
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genid
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/order
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protodesc
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/descriptorpb
google.golang.org/protobuf/types/known/anypb
google.golang.org/protobuf/types/known/durationpb
google.golang.org/protobuf/types/known/timestamppb
# gopkg.in/yaml.v2 v2.4.0
gopkg.in/yaml.v2
