package main

import (
	"log"

	"gitlab.com/cloud_native_architecture/popcorn_review/api_gateway/internal/apigateway"
)

func main() {

	defer apigateway.Close()
	// Start server which listening
	if err := apigateway.GetServer().Serve(); err != nil {
		log.Fatalln(err)
	}

}
